/**
 * Created by Howell on 11/4/17.
 */
'[nocompress]';
define(['jquery', 'tisson.ui', 'tisson.lang', 'tisson.http', 'jweixin'], function ($, UI, Lang, HTTP, wx) {
    function Demo() {
        console.log("init");
    }

    Demo.prototype.initApp = function () {
        var self = this;
        this.initWXConfig();

        $("#id_Share").click(function () {
           alert("123");
        });

        $("#id_WCPay").click(function () {
            HTTP.callWebService({
                'service': '&CDN_Url&/payMchPayServlet',
                'params': {},
                'showLoading': false,
                'async': false,
                'success': function (result) {
                    self.WCPay(result)
                }
            });
        });

        $("#id_Pay").click(function () {

            HTTP.callWebService({
                'service': '&CDN_Url&/payJSServlet',
                'params': {},
                'showLoading': false,
                'async':false,
                'success': function(result){
                    alert('pay2');
                    alert(JSON.stringify(result));
                    wx.chooseWXPay({
                        timestamp : Number(result["timeStamp"]),
                        nonceStr : result["nonceStr"],
                        package : result["package"],
                        signType : result["signType"],
                        paySign : result["paySign"],

                        success : function (res) {
                            // 支付成功后的回调函数
                            alert("支付成功:"+JSON.stringify(res));
                        }
                    });
                }
            });
        });
    };

    Demo.prototype.initWXConfig = function() {

        wx.error(function(res){ alert("接口处理失败验证:"+JSON.stringify(res)); });
        HTTP.callWebService({
            'service': '&CDN_Url&/getConfigJson',
            'params': {"url": window.location.href},
            'showLoading': false,
            'async':false,
            'success': function(result){
                alert("init WXConfig成功:"+result);
                wx.config(result);
                wx.ready(function(){ alert("接口处理成功验证！"); });
                wx.error(function(res){ alert("接口处理失败验证:"+JSON.stringify(res)); });
            }
        });
    };


    Demo.prototype.WCPay = function (x_json) {
        function onBridgeReady(){
            WeixinJSBridge.invoke(
                'getBrandWCPayRequest', x_json ,
                function(res){
                    if(res.err_msg == "get_brand_wcpay_request：ok" ) {}     // 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回    ok，但并不保证它绝对可靠。
                }
            );
        }
        if (typeof WeixinJSBridge == "undefined"){
            if( document.addEventListener ){
                document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
            }else if (document.attachEvent){
                document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
            }
        }else{
            onBridgeReady();
        }
    };



    return new Demo();
});