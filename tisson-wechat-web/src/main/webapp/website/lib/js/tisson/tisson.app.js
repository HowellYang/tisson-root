/**
 * Created by Howell on 2016/9/25.
 * Email:th15817161961@gmail.com
 * 公共js: angular init
 */
define("tisson.app",["angular","angularUIRouter","jquery","angularCSS","bootstrap","crypto"], function() {

    var app = angular.module('tisson.app', ['ui.router','angularCSS']);
    app.config(["$sceDelegateProvider",function ($sceDelegateProvider) {
        // $sceDelegateProvider.resourceUrlWhitelist([
        //     // Allow same origin resource loads.
        //     'self'
        // ]);
    }]);
    window.tissonApp = app;

    return app;
});

