/**
 * Created by Howell on 2016/9/21.
 * Email:th15817161961@gmail.com
 * 公共js: 前端js入口，配置require
 *
 */
var _url = window.location.href,
    _subclass = _url.substring(0, _url.indexOf('.html')+5);
    _subclass = _subclass.substring(_subclass.lastIndexOf('/') + 1, _subclass.indexOf('.html'));
function getRandNumber() {
    var rand = Math.round(Math.random()*899999+100000);
    return rand;
}

// 移动端适配方案 flexible
!function(a,b){function c(){console.log("base:flexible1");var b=f.getBoundingClientRect().width;b/i>540&&(b=540*i);var c=b/9;f.style.fontSize=c+"px",k.rem=a.rem=c;console.log("base:flexible2");}var d,e=a.document,f=e.documentElement,g=e.querySelector('meta[name="viewport"]'),h=e.querySelector('meta[name="flexible"]'),i=0,j=0,k=b.flexible||(b.flexible={});if(g){console.warn("将根据已有的meta标签来设置缩放比例");var l=g.getAttribute("content").match(/initial\-scale=([\d\.]+)/);l&&(j=parseFloat(l[1]),i=parseInt(1/j))}else if(h){var m=h.getAttribute("content");if(m){var n=m.match(/initial\-dpr=([\d\.]+)/),o=m.match(/maximum\-dpr=([\d\.]+)/);n&&(i=parseFloat(n[1]),j=parseFloat((1/i).toFixed(2))),o&&(i=parseFloat(o[1]),j=parseFloat((1/i).toFixed(2)))}}if(!i&&!j){var p=a.navigator.userAgent,q=(!!p.match(/android/gi),!!p.match(/iphone/gi)),r=q&&!!p.match(/OS 9_3/),s=a.devicePixelRatio;i=q&&!r?s>=3&&(!i||i>=3)?3:s>=2&&(!i||i>=2)?2:1:1,j=1/i}if(f.setAttribute("data-dpr",i),!g)if(g=e.createElement("meta"),g.setAttribute("name","viewport"),g.setAttribute("content","initial-scale="+j+", maximum-scale="+j+", minimum-scale="+j+", user-scalable=no"),f.firstElementChild)f.firstElementChild.appendChild(g);else{var t=e.createElement("div");t.appendChild(g),e.write(t.innerHTML)}a.addEventListener("resize",function(){clearTimeout(d),d=setTimeout(c,300)},!1),a.addEventListener("pageshow",function(a){a.persisted&&(clearTimeout(d),d=setTimeout(c,300))},!1),"complete"===e.readyState?e.body.style.fontSize=12*i+"px":e.addEventListener("DOMContentLoaded",function(){e.body.style.fontSize=12*i+"px"},!1),c(),k.dpr=a.dpr=i,k.refreshRem=c,k.rem2px=function(a){var b=parseFloat(a)*this.rem;return"string"==typeof a&&a.match(/rem$/)&&(b+="px"),b},k.px2rem=function(a){var b=parseFloat(a)/this.rem;return"string"==typeof a&&a.match(/px$/)&&(b+="rem"),b}}(window,window.lib||(window.lib={}));


// 浏览器调试安全提示！
!function(e){function o(s){if(t[s])return t[s].exports;var n=t[s]={exports:{},id:s,loaded:!1};return e[s].call(n.exports,n,n.exports,o),n.loaded=!0,n.exports}var t={};return o.m=e,o.c=t,o.p="",o(0)}([function(e,o,t){e.exports=t(1)},function(e,o){!function(){var e;if(window.console&&"undefined"!=typeof console.log){try{(window.parent.__has_console_security_message||window.top.__has_console_security_message)&&(e=!0)}catch(o){e=!0}if(window.__has_console_security_message||e)return;var t=" 温馨提示：请不要调皮地在此粘贴执行任何内容，这可能会导致您的账户受到攻击，给您带来损失 ！._.",s="^_^",n="",i=[s," ",n].join("");/msie/gi.test(navigator.userAgent)?(console.log(t),console.log(i)):(console.log("%c Tisson %c Copyright \xa9 2011-%s",'font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:64px;color:#ec7500;-webkit-text-fill-color:#ec7500;-webkit-text-stroke: 1px #ec7500;',"font-size:12px;color:#999999;",(new Date).getFullYear()),console.log("%c "+t,"color:#333;font-size:16px;"),console.log("\n "+i)),window.__has_console_security_message=!0}}()}]);

// 判断 IE7 浏览器，跳转到提示升级浏览器的页面
if(document.querySelector == null){
    location.href = "/static/UpdateBrowser.html";
}

// 处理IE8，Array不支持indexOf的方法。
if (!Array.prototype.indexOf){
    Array.prototype.indexOf = function(elt /*, from*/){
        var len = this.length >>> 0;
        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;
        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

function urlParameters(urlParame) {
    var url;
    if(urlParame==null){
        url=window.location.href;
    }else{
        url=urlParame;
    }
    if (url.lastIndexOf("#") != -1) {
        url = url.substring(0, url.lastIndexOf("#"));
    }
    var para = "";
    var retJson={};
    if (url.lastIndexOf("?") > 0) {
        para = url.substring(url.lastIndexOf("?") + 1, url.length);
        var arr = para.split("&");
        para = "";
        for (var i = 0; i < arr.length; i++) {
            retJson[arr[i].split("=")[0]]=arr[i].split("=")[1];
        }
        console.log(JSON.stringify(retJson));
    } else {
        console.log("没有参数!");
    }
    return retJson;
}


require.config({
    baseUrl : "js",
    waitSeconds: 0,
    paths : {
        'tisson.app' : '&CDN_Url&/lib/js/tisson/tisson.app',
        'tisson.lang' : '&CDN_Url&/lib/js/tisson/tisson.lang',
        'tisson.ui' : '&CDN_Url&/lib/js/tisson/tisson.ui',
        'tisson.http' : '&CDN_Url&/lib/js/tisson/tisson.http',
        'tisson.global' : '&CDN_Url&/lib/js/tisson/tisson.global',


        'crypto' : '&CDN_Url&/lib/js/thirdParty/crypto-js',

        'jquery' : '&CDN_Url&/lib/js/thirdParty/jquery-1.12.4.min',
        'jqueryDataTables' : '&CDN_Url&/lib/js/thirdParty/jquery.dataTables.min',
        'angular' : '&CDN_Url&/lib/js/thirdParty/angular.min',
        'angularUIRouter': '&CDN_Url&/lib/js/thirdParty/angular-ui-router.min',
        'angularCSS' : '&CDN_Url&/lib/js/thirdParty/angular-css.min',
        'header.router' : '&CDN_Url&/lib/templates/header/js/header.router',
        'bootstrap':'&CDN_Url&/lib/js/thirdParty/bootstrap.min',
        'bootstrapDatetimepicker':'&CDN_Url&/lib/js/thirdParty/bootstrap-datetimepicker.min',
        'bootstrapDatetimepickerZhCN':'&CDN_Url&/lib/js/thirdParty/bootstrap-datetimepicker.zh-CN',

        'jweixin':'http://res.wx.qq.com/open/js/jweixin-1.0.0',
        //'highcharts':'/lib/js/thirdParty/highcharts.min',
        //'highchartsExporting' : '/lib/js/thirdParty/highcharts.exporting.min',

        // 子应用特有
        'subconfig' : 'config',
        'subclass'  : _subclass
    },
    shim: {
        angular: ['jquery'],
        angularUIRouter: ['angular'],
        angularCSS: ['angular'],
        //highcharts:['jquery'],
        //highchartsExporting:['highcharts'],
        bootstrap:['angular','jquery'],
        jqueryDataTables:['jquery'],
        bootstrapDatetimepicker:['bootstrap','jquery'],
        bootstrapDatetimepickerZhCN:['bootstrap','jquery','bootstrapDatetimepicker']
    },
    urlArgs : function (id, url) {
        var args = "v=&version&";
        if( id == 'jquery' ||
            id == 'jqueryDataTables' ||
            id == 'angular' ||
            id == 'jweixin' ||
            id == 'angularUIRouter' ||
            id == 'bootstrap' ||
            id == 'angularCSS' ||
            id == 'bootstrapDatetimepicker' ||
            id == 'bootstrapDatetimepickerZhCN' ||
            id == 'highchartsExporting'
        ){
            args = "v=1.0.0";
        }
        return (url.indexOf('?') === -1 ? '?' : '&') + args;
    }
});

if ('&debug&' != 'true') {
    console.log = function () {};
    console.info = function () {};
    console.error = function() {};
}
console.log("init : "+_subclass);

'use strict';
require(['tisson.global', 'subconfig'], function(config, subconfig) {
    // 将模块中的配置覆盖全局配置
    for (var prop in subconfig) {
        config[prop] = subconfig[prop];
    }
    window.config = config;

    require(['subclass'], function(subclass) {
        //由 "subclass" 去渲染路由配置
        // tissonApp.config(['$cssProvider',function ($cssProvider) {
        //     angular.extend($cssProvider.defaults, {
        //         container: 'head',
        //         method: 'append',
        //         persist: true,
        //         preload: true,
        //         bustCache: false
        //     });
        // }]);

        //渲染
        // angular.bootstrap(document, ['tisson.app']);
        subclass.initApp();
    });
});



