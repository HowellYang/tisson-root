/**
 * Created by zhangxue on 2017年03月27日11:49:28.
 * Version 1.0
 */

define(['jquery', 'tisson.lang'], function ($, Lang) {

    /**
     *
     * @param inputId
     * @param inputType (number|password|mobile|bankCard|IDCard|EMail|phone|AlphaNumeric|sanCode)
     * @param funcInput
     * @param funcClear
     * @param decimal (inputType is number,the number after the decimal point)
     * @param realTimeVeri (Whether or not real time verification)
     * @constructor
     */
    function InputText (inputId, inputType, funcInput, funcClear, decimal,realTimeVeri) {
        this.inputId = inputId;
        this.btnClear = null;
        this.inputText = null;
        this.divWrap = null;
        this.value = "";
        this.funcInput = funcInput;
        this.funcClear = funcClear;
        this.inputType = inputType;
        this.decimal = decimal
        this.realTimeVeri = realTimeVeri;
        this.init();

        if(this.inputType == 'password'){
            this.setPassword();
        }
    }

    InputText.prototype.init = function() {
        var self = this;
        self.inputText = document.getElementById(self.inputId);
        self.divWrap = self.inputText.parentNode.parentNode;
        self.btnClear = $(self.inputText.parentNode).next()[0];
        self.setFormat();

        /*self.btnClear.onclick = function () {
         self.inputText.value = '';
         $(this).hide();
         if (Lang.isFunction(self.funcClear)) {
         self.funcClear();
         console.log(" in lib clear");
         }
         };*/
        self.btnClear.addEventListener('click',function () {
            self.inputText.value = '';
            $(this).hide();
            if (Lang.isFunction(self.funcClear)) {
                self.funcClear();
                console.log(" in lib clear");
            }
        });
        self.inputText.addEventListener('input', function() {
            if(self.realTimeVeri == false) {
                self.value = $(this).val().toString();
            }else{
                self.setInputType(self.inputText.value, this);
                self.value = $(this).val().toString();
                if (Lang.isFunction(self.funcInput)) {
                    self.funcInput(self.value);
                }
            }
            if (self.value.length > 0) {
                $(self.btnClear).show();
            } else {
                $(self.btnClear).hide();
            }
        }, false);

        $(self.inputText).focus(function () {
            $(self.inputText).val($(self.inputText).val().replace(/\s/g,''));
            self.divWrap.style.borderColor = '#458ECB';
        }).blur(function () {
            self.divWrap.style.borderColor = '#C4C5C7';
        });

        if(this.inputType == 'phone'){
            $(self.inputText.parentNode.parentNode).find('.btn-phone').click(function(){
                Tisson.Contacts.openContacts(function (phone_no) {
                    var phone_val = phone_no.replace(/[^0-9]+/g,'').replace(/\s/g,'');
                    if(phone_val.length != 11){
                        Tisson.Toast.makeText('请选择正确的手机号', Tisson.Toast.LENGTH_SHORT);
                        return;
                    }
                    self.setValue(phone_val.replace(/(\d{3})(\d{4})/g,"$1 $2 "));
                    self.funcInput(phone_val);
                }, function (error) {
                    Tisson.Dialog.alert(error);
                });
            });
        }

        if(this.inputType == "sanCode"){
            $(self.inputText).data("spanCode","");
            $(self.divWrap).find("div[class=scan-code]").click(function(){
                Tisson.Scanner.getBarCode(function(resultData){
                    $(self.inputText).data("spanCode",JSON.stringify(resultData).replace(/\"/g,''));
                    $(self.inputText).val(JSON.stringify(resultData).replace(/\"/g,''));
                    if( $(self.inputText).val().toString().trim() != "")
                        $(self.btnClear).show();
                    else
                        $(self.btnClear).hide();
                });
            });
        }

    };

    InputText.prototype.getValue = function() {
        if(this.inputType == 'password'){
            return PasswordKeyBoard.keyStr;
        }
        return this.inputText.value;
    };

    InputText.prototype.getToEmptyValue = function() {
        if(this.inputType == 'password'){
            return PasswordKeyBoard.keyStr;
        }
        return this.inputText.value.replace(/\s/g,'');
    };

    InputText.prototype.setValue = function(value) {
        this.inputText.value = value;
        $(this.btnClear).show();
    };

    InputText.prototype.clearValue = function(){
        if(this.inputType == 'password'){
            PasswordKeyBoard.keyStr = '';
        }
        this.value = "";
        this.inputText.value = "";
        $(this.btnClear).hide();
    };

    InputText.prototype.setPlaceholder = function(ph) {
        this.inputText.setAttribute('placeholder', ph);
    };

    /**
     * Restricted content
     * @param val : input value
     * @param valObj : input DOM object
     */
    InputText.prototype.setInputType = function(val,valObj){
        var self = this;
        if (self.inputType == null || self.inputType == 'undefined') {
            return;
        }
        switch(self.inputType){
            case 'number':
                if(self.decimal == null || self.decimal == undefined){
                    $(valObj).val($(valObj).val().replace(/[^0-9]+/,''));
                } else {
                    var thisVal = $(valObj).val();
                    if(thisVal.indexOf('.') == 0 || (thisVal.indexOf('.') >0 && (thisVal.split('.').length > 2 || thisVal.split('.')[1].length > self.decimal*1)) ){
                        $(valObj).val(thisVal.substring(0,thisVal.length-1));
                    }
                    if(thisVal.indexOf('0') == 0 && thisVal.length > 1 && thisVal.indexOf('.') != 1){
                        $(valObj).val(thisVal.substring(0,thisVal.length-1));
                    }
                    $(valObj).val($(valObj).val().replace(/[^0-9.]+/,''));
                }
                break;
            case 'mobile':
            case 'bankCard':
            case 'phone':
                $(valObj).val($(valObj).val().replace(/[^0-9]+/,''));
                break;
            case 'IDCard':
                $(valObj).val($(valObj).val().replace(/[^0-9Xx]+/,""));
                break;
            case 'AlphaNumeric':
                $(valObj).val($(valObj).val().replace(/[^0-9a-zA-Z]+/,""));
                break;
            case 'sanCode':
                $(valObj).val($(valObj).val().replace(/[^0-9\.\-]+/,""));
                break;
            default :
                $(valObj).val($(valObj).val().replace(/[^0-9]+/,''));
                break;
        }
    };

    /**
     * According to the type of formatted content
     */
    InputText.prototype.setFormat = function(){
        var self = this;
        if (self.inputType == null || self.inputType == 'undefined') {
            return;
        }
        var inputObj = $(self.inputText);
        switch(self.inputType){
            case 'mobile':
                inputObj.blur(function(){
                    inputObj.val(inputObj.val().replace(/\s/g,'').replace(/(\d{3})(\d{4})/g,"$1 $2 "));
                });
                break;
            case 'phone':
                inputObj.blur(function(){
                    inputObj.val(inputObj.val().replace(/\s/g,'').replace(/(\d{3})(\d{4})/g,"$1 $2 "));
                });
                break;
            case 'bankCard':
                inputObj.blur(function(){
                    inputObj.val(inputObj.val().replace(/\s/g,'').replace(/(\d{4})(?=\d)/g,"$1 "));
                });
                break;
        }
    };

    /**
     * init native PINpad
     */
    InputText.prototype.PasswordMoveDown = function() {
        window.scrollBy(0,200);
    };
    InputText.prototype.setPassword = function(){
        var self = this;
        PasswordKeyBoard.initPwdId(self.inputId, function(result){
            if (Lang.isFunction(self.funcInput)) {
                self.funcInput(result);
            }
            if(result.length>0){
                $(self.btnClear).show();
            }else{
                $(self.btnClear).hide();
            }

            self.btnClear.onclick = function () {
                var me = $(this);
                setTimeout(function(){
                    self.inputText.value = '';
                }, 300);
                setTimeout(function(){
                    PasswordKeyBoard.keyStr = '';
                }, 200);
                setTimeout(function(){
                    me.hide();
                }, 100);
            };

        }, 12);//初始化密码键盘；
        document.getElementById(self.inputId).onclick = function(){
            PasswordKeyBoard.popPswKeyboard();
            PasswordKeyBoard.keyInputId = self.inputId;
            self.PasswordMoveDown();
        };
    };

    /**
     *  Calibration values
     * @returns {boolean}
     */
    InputText.prototype.getInputCheck = function(){
        var self = this;
        if (self.inputType == null || self.inputType == 'undefined') {
            return;
        }
        var inputVal = $(self.inputText).val().replace(/\s/g,'');
        switch(self.inputType){
            case 'mobile':
                if(!inputVal.match("^((13[0-9])|(14[5,7,9])|(15[^4,\\D])|(18[0-9])|(17[5-9]))\\d{8}$")){
                    return false;
                }
                return true;
                break;
            case 'EMail':
                if(!inputVal.match("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$")){
                    return false;
                }
                return true;
                break;
            case 'IDCard':
                if(!inputVal.match("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}(\\d|x|X)$")){
                    return false;
                }
                return true;
                break;
        }
    };

    InputText.prototype.checkInputType = function(){
        var self = this;
        if (self.inputType == null || self.inputType == 'undefined') {
            return;
        }
    };

    //
    function BlockRadioGroup(groupId, defaultChecked, callback) {
        this.groupId = groupId;
        this.group = null;
        this.checkedItem = null;
        this.allItem = null;
        this.normalClassName = 'block-radio-normal';
        this.checkClassName = 'block-radio-checked';
        this.disabledClassName = 'block-radio-disabled';
        this.callback = callback;
        this.defaultChecked = defaultChecked;
        this.init();
        if (defaultChecked !== null && defaultChecked !== 'undefined') {
            this.setChecked(defaultChecked);
        }
    }

    BlockRadioGroup.prototype.init = function() {
        var self = this;
        self.group = document.getElementById(self.groupId);
        self.allItem = $(self.group).children();

        // 为每个item绑定点击事件
        for (var i = 0; i < self.allItem.size(); i ++) {
            self.allItem[i].onclick = function() {
                self.setChecked(this);
                if (self.callback!= null && self.callback!= 'undefined') {
                    self.callback($(this).data('value'),i,this);
                }
                self.defaultChecked = $(this).attr('id');
            };
        }
    };
    BlockRadioGroup.prototype.setChecked = function(elt) {
        var self = this;
        if (typeof elt === 'string') {
            elt = document.getElementById(elt);
        }
        if (self.checkedItem === elt) {
            return;
        }
        if (self.checkedItem !== null) {
            $(self.checkedItem).removeClass(self.checkClassName).addClass(self.normalClassName);
        }
        $(elt).removeClass(self.normalClassName).addClass(self.checkClassName);
        self.checkedItem = elt;
    };
    BlockRadioGroup.prototype.setAllNormal = function() {
        if (this.checkedItem !== null) {
            $(self.checkedItem).removeClass(self.checkClassName).addClass(self.normalClassName);
        }
    };
    BlockRadioGroup.prototype.getCheckedItem = function() {
        return this.checkedItem;
    };
    BlockRadioGroup.prototype.getCheckedValue = function() {
        return $(this.checkedItem).data('value');
    };
    BlockRadioGroup.prototype.setDisabled = function(val, selectArray) {
        var self = this;
        if(val == false){
            self.init();
            for (var i = 0; i < self.allItem.size(); i ++) {
                $(self.allItem[i]).removeClass(self.disabledClassName).addClass(self.normalClassName);
            }
            $(self.defaultChecked).removeClass(self.normalClassName).addClass(self.checkClassName);
        }else if(val == true){
            self.defaultChecked = self.checkedItem;
            if(selectArray != null){
                for(var i=0; selectArray.length > i; i++){
                    self.allItem[selectArray[i]].onclick = function () {};
                    $(self.allItem[selectArray[i]]).removeClass(self.checkClassName).addClass(self.disabledClassName);
                    $(self.allItem[selectArray[i]]).removeClass(self.normalClassName).addClass(self.disabledClassName);
                }
            }else {
                // 为每个item绑定点击事件
                for (var i = 0; i < self.allItem.size(); i++) {
                    self.allItem[i].onclick = function () {};
                    $(self.allItem[i]).removeClass(self.checkClassName).addClass(self.disabledClassName);
                    $(self.allItem[i]).removeClass(self.normalClassName).addClass(self.disabledClassName);
                }
            }
        }
    };

    /**
     * html 模板
     */
    function Template(){}

    /**
     * one to one template
     * @param template_id : object id
     * @param data : JSONObject
     */
    Template.prototype.template_OneToOne=function(template_id, data){
        var tempObj = document.getElementById(template_id);
        var tempObj_show = document.getElementById(template_id+"_show");
        var newHTML = tempObj.innerHTML.replace(/\$\w+\$/gi, function(matchs) {
            var returns = data[matchs.replace(/\$/g, "")];
            return (returns + "") == "undefined"? "": returns;
        });
        tempObj_show.innerHTML = newHTML;
    };

    /**
     * more to one template
     * @param from_id : object id
     * @param to_id : object id
     * @param array_data : JSONObject Array
     */
    Template.prototype.template_MoreToOne = function(from_id, to_id, array_data) {
        var from_Obj = document.getElementById(from_id);
        var to_obj = document.getElementById(to_id);
        for(var i=0;array_data.length>i;i++){
            var newHTML = from_Obj.innerHTML.replace(/\$\w+\$/gi, function(matchs) {
                var returns = array_data[i][matchs.replace(/\$/g, "")];
                return (returns + "") == "undefined"? "": returns;
            });
            to_obj.innerHTML += newHTML;
        }
    };

    /**
     * @param id String
     * @param contJson {key : string,}
     * @param titleStr String
     * @param btnJson {key : func,}
     * @param type String （dialog|list）
     * @param callBack func
     * @param selectItemObj String
     * @constructor
     */
    function DialogClass(id, contJson, titleStr ,btnJson ,type ,callBack,selectItemObj) {
        this.id = id;
        this.contJson = contJson;
        this.titleStr = titleStr;
        this.btnJson = btnJson;
        this.type = type;
        this.dialogClass_bg = null;
        this.contOutDiv = null;
        this.callBack = callBack;
        this.selectItemObj = selectItemObj;
        if($('#' + this.id).html().replace(/\s/g,'').length == 0){
            this.init();
        }else{
            $('#' + this.id).show();
        }
    };

    DialogClass.prototype.init = function() {
        this.thisDom = document.getElementById(this.id);
        this.dialogClass_bg = document.createElement("div");
        this.dialogClass_bg.className = "dialogClass-bg";
        var self = this;
        if(self.type = "list")
            self.dialogClass_bg.onclick = function(){back();};

        //Add Transparent background
        this.thisDom.appendChild(this.dialogClass_bg);
        this.contOutDiv = document.createElement("div");
        //set title
        if(this.titleStr != null){
            this.setTitle();
        }
        //Add content
        switch (this.type){
            case 'dialog':
                this.contOutDiv.className = "dialogClass-content";
                this.setContent();
                break;
            case 'list':
                this.contOutDiv.className = "dialogClass-list-content box box-ver";
                this.setListContent();
                break;
        };
        //Add button
        if(this.btnJson != null){
            this.setButton();
        }
        //Adding outside content div
        this.thisDom.appendChild(this.contOutDiv);
    };

    DialogClass.prototype.setTitle = function() {
        var titleDiv = document.createElement("pre");
        titleDiv.className = "dialogClass-title";
        titleDiv.innerHTML = this.titleStr;
        this.contOutDiv.appendChild(titleDiv);
    };

    DialogClass.prototype.setTitlePublic = function(biogTitle) {
        $('#' + this.id +' .dialogClass-title').html(biogTitle);
    };

    DialogClass.prototype.setContent = function() {
        this.itemOutDiv = document.createElement("div");
        this.itemOutDiv.className = "dialogClass-div-item";
        for(var i in this.contJson){
            var itemDiv = document.createElement("div");
            itemDiv.className = "dialogClass-item";
            itemDiv.innerHTML = i +":"+ this.contJson[i];
            this.itemOutDiv.appendChild(itemDiv);
        }
        this.contOutDiv.appendChild(this.itemOutDiv);
    };

    DialogClass.prototype.setListContent = function(){
        var self = this;
        this.itemOutDiv = document.createElement("div");
        this.itemOutDiv.className = "box-f1 dialogClass-item-out";
        for(var i in this.contJson){
            var itemDiv = document.createElement("div");
            itemDiv.className = "dialogClass-list-item";
            itemDiv.innerHTML = i;
            itemDiv.setAttribute("data-val",self.contJson[i]);
            var itemImg = document.createElement("div");
            itemImg.className = "dialogClass-list-img-off";

            if(self.selectItemObj == self.contJson[i]){
                itemImg.className = "dialogClass-list-img-on";
                self.selectItemObj = itemDiv;
            }
            itemDiv.onclick = function(){
                if(self.selectItemObj != null){
                    $(self.selectItemObj).find('div').removeClass("dialogClass-list-img-on").addClass("dialogClass-list-img-off");
                }

                $(this).find('div').addClass("dialogClass-list-img-on");
                self.callBack(this.getAttribute("data-val"),this);
                self.selectItemObj = this;
            };
            itemDiv.appendChild(itemImg);
            this.itemOutDiv.appendChild(itemDiv);
        }
        this.contOutDiv.appendChild(this.itemOutDiv);
    };

    DialogClass.prototype.setButton = function() {
        var btnOutDiv = document.createElement("div");
        btnOutDiv.className = "box dialogClass-div-btn";
        for(var i in this.btnJson){
            var btnDiv = document.createElement("div");
            btnDiv.className = "box-f1 dialogClass-btn dialogClass-btn-goahead";
            btnDiv.innerHTML = i ;
            btnDiv.onclick = this.btnJson[i];
            btnOutDiv.appendChild(btnDiv);
        }
        this.itemOutDiv.appendChild(btnOutDiv);
    };

    DialogClass.prototype.setButtonClass = function(item ,className) {
        $(".dialogClass-btn:eq("+ [item-1] +")").addClass(className);
    };

    DialogClass.prototype.setItemClass = function(item ,className){
        $(".dialogClass-item:eq("+ [item-1] +")").addClass(className);
    };

    DialogClass.prototype.clearDialog = function(callBack) {
        this.thisDom.innerHTML = "";
        if(callBack != null && callBack != 'undefined'){
            callBack();
        }
    };

    DialogClass.prototype.hideDialog = function(callBack) {
        $('#' + this.id).hide();
        if(callBack != null && callBack != 'undefined'){
            callBack();
        }
    };

    /**
     * Drop Down
     * @param id String
     * @param thisObj obj
     * @param conJson JSON
     * @param callBack  Func
     * @param funcBoxBG Func
     */
    function dropDownBox(id, thisObj, conJson, callBack, funcBoxBG){
        this.id = id;
        this.thisObj = thisObj;
        this.contJson = conJson;
        this.callBack = callBack;
        this.funcBoxBG = funcBoxBG;
        if(this.thisObj != null){
            this.boxWidth = $(this.thisObj).width();
            this.boxHeight = $(this.thisObj).height();
            this.boxTopHeight = this.thisObj.offsetTop;
            this.boxLeft = this.thisObj.offsetLeft;
        }

        $('#' + this.id).html("") ;
        this.init();
    }

    dropDownBox.prototype.init = function(){
        var self =this;
        this.thisDom = document.getElementById(this.id);
        this.dropDownBox_bg = document.createElement("div");
        this.dropDownBox_bg.className = "dropDownBox-bg";
        this.dropDownBox_bg.onclick = function(){
            self.funcBoxBG();
        };
        //Add Transparent background
        this.thisDom.appendChild(this.dropDownBox_bg);
        this.contOutDiv  = document.createElement("div");
        this.contOutDiv.className = "dropDownBox-list-content";
        $(this.contOutDiv).width(this.boxWidth + 'px');
        this.contOutDiv.style.top = (this.boxTopHeight + this.boxHeight + 3) + 'px';
        this.contOutDiv.style.left = this.boxLeft + 'px';
        console.log(JSON.stringify(this.contJson));
        this.setListContent();
        this.thisDom.appendChild(this.contOutDiv);
    };

    dropDownBox.prototype.setListContent = function(){
        var self = this;
        this.itemOutDiv = document.createElement("div");
        for(var i in this.contJson){
            var itemDiv = document.createElement("div");
            itemDiv.className = "dropDownBox-list-item";
            itemDiv.innerHTML = i;
            itemDiv.setAttribute("data-val",self.contJson[i]);
            itemDiv.onclick = function(){
                self.callBack(this.getAttribute("data-val"),this);
            };
            this.itemOutDiv.appendChild(itemDiv);
        }
        this.contOutDiv.appendChild(this.itemOutDiv);
    };


    dropDownBox.prototype.clearDropDownBox = function(callBack) {
        this.thisDom.innerHTML = "";
        if(callBack != null && callBack != 'undefined'){
            callBack();
        }
    };

    dropDownBox.prototype.hideDropDownBox = function(callBack) {
        $('#' + this.id).hide();
        if(callBack != null && callBack != 'undefined'){
            callBack();
        }
    };

    var showDialogClass = function(type, callBack, msgJson) {
        this.type = type;
        this.callBack = callBack;
        this.msgJson = msgJson;
        this.init();
    };

    showDialogClass.prototype.init = function() {
        var self = this;
        switch (this.type){
            case 'showDateChoiceDialog':
                Tisson.Dialog.showDateChoiceDialog(Lang.getDate_YYYY_MM_DD(),this.callBack);
                break;
            case 'showDateModifyDialog':
                Tisson.Dialog.showDateChoiceDialog(self.msgJson,this.callBack);
                break;
        }
    };


    return {
        'InputText' : InputText,
        'BlockRadioGroup' : BlockRadioGroup,
        'dropDownBox' : dropDownBox,
        'DialogClass' : DialogClass,
        'showDialogClass' : showDialogClass,
        'Template':Template
    };
});