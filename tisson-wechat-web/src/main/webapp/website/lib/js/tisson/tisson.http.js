/**
 * Created by Howell on 2016/9/25.
 * Email:th15817161961@gmail.com
 * 公共js: 公共数据交互
 */
define(['tisson.lang',"tisson.ui"],function(Lang, UI) {
    function Http() {

    }
    Http.prototype.initApp = function () {

    };


    Http.prototype.setCommonParams = function (params) {
        params.WebKeep = Lang.getKeep();

        return params;
    };

    Http.prototype.getSecurityScriptRD = function () {
        var SecurityScriptRD = "";
        this.callWebService({
            'service': '/api/security/random',
            'params': {},
            'showLoading': false,
            'async':false,
            'success': function(result){SecurityScriptRD =result['SecurityScriptRD'];}
        });
        return SecurityScriptRD;
    };





    /**
     * Ajax call to Web Service
     * @param settings 字段：
     * {
     *  service : 请求地址,
     *  params : 请求参数,
     *  success :,
     *  error :,
     *  showLoading :,
     *  setTime :,
     *  async :,
     *  openInject :
     * }
     */
    Http.prototype.callWebService = function(settings){
        config.isOpen = true;
        var callSelf = this;
        var url = null;
        // if (config.loadingDialog == null){
        //     config.loadingDialog = UI.LoadingDialog();
        // }
        // if (settings.showLoading != null && settings.showLoading == true) {
        //     config.loadingDialog.showLoading();
        // }

        if(settings.setTime == null){
            settings.setTime = 75000;
        }

        if(settings.openInject == null){
            settings.openInject = false;
        }

        if(settings.dataType == null){
            settings.dataType = "json";
        }

        if (typeof settings.error !== 'function') {
            settings.error = function () {
                console.log("error 请求失败: " + url);
            }
        }
        var successCallBack = function(result) {
            // if (settings.showLoading != null && settings.showLoading == true) {
            //     config.loadingDialog.hideLoading();
            // }
            console.log('successCallBack response: ' + JSON.stringify(result));
            config.isOpen = true;
            if(settings.openInject){
                var global_url = '/lib/js/tisson/tisson.global.js?v=' + Date.parse(new Date());
                require([global_url],function(config){
                    for (var prop in config) {
                        window.config[prop] = config[prop];
                    }
                    settings.success(result);
                });
            } else {
                settings.success(result);
            }
        };


        var errorCallBack = function(result){
            // if (settings.showLoading != null && settings.showLoading == true) {
            //     config.loadingDialog.hideLoading();
            // }
            console.log('successCallBack response: ' + JSON.stringify(result));
            config.isOpen = true;

            settings.error(result);
        };

        var suffix = '?ran' + Math.floor(Math.random() * 100 + 1) + '=' + Date.parse(new Date());

        if(settings.URL != null){
            url = settings.URL + settings.service + suffix;
        } else {
            url = settings.service + suffix;
        }
        if(settings.async == null){
            settings.async = true;
        }
        console.log('isOpen: ' + config.isOpen);
        if(config.isOpen == false){
            return;
        }else{
            config.isOpen = false;
        }
        console.log('url: ' + url);

        var ActionCall = function () {
            try {
                if (typeof settings.params === 'object') {
                    settings.params = JSON.stringify(settings.params);
                }
                console.log('request: ' + settings.params);

                window.jqXHR = $.ajax({
                    url: url,
                    type: "POST",
                    async: settings.async,
                    data: settings.params,
                    timeout: settings.setTime,
                    contentType: "application/json; charset=utf-8",
                    dataType: settings.dataType,
                    success: successCallBack,
                    error: errorCallBack
                });
            } catch (e) {
                console.error('try catch error: ' + e.message);
                config.isOpen = true;
                settings.error({"msg":e.message});
            }
        };

        if(settings.params["isCheckDeviceInfo"] != null && settings.params["isCheckDeviceInfo"] == "Y"){
            callSelf.getDeviceInfo(function(params){
                //硬件信息
                settings.params["MachineNetwork"] = params['MachineNetwork'];
                settings.params["MachineDisk"] = params['MachineDisk'];
                settings.params["MachineCPU"] = params['MachineCPU'];
                ActionCall();
            });
        } else {
            ActionCall();
        }

    };


    return new Http();
});