define(['jquery', 'tisson.ui', 'tisson.lang', 'tisson.http'], function ($, UI, Lang, HTTP) {

    var onlineOrderSelf = null;
    var wx =null;
    function OnlineOrder() {
        onlineOrderSelf = this;
    };

    // wx.config({
    //     debug: false,
    //     appId: 'wxf8b4f85f3a794e77',
    //     timestamp: 1420774989,
    //     nonceStr: '2nDgiWM7gCxhL8v0',
    //     signature: '1f8a6552c1c99991fc8bb4e2a818fe54b2ce7687',
    //     jsApiList: [
    //         'checkJsApi',
    //         'onMenuShareTimeline',
    //         'onMenuShareAppMessage',
    //         'onMenuShareQQ',
    //         'onMenuShareWeibo',
    //         'hideMenuItems',
    //         'showMenuItems',
    //         'hideAllNonBaseMenuItem',
    //         'showAllNonBaseMenuItem',
    //         'translateVoice',
    //         'startRecord',
    //         'stopRecord',
    //         'onRecordEnd',
    //         'playVoice',
    //         'pauseVoice',
    //         'stopVoice',
    //         'uploadVoice',
    //         'downloadVoice',
    //         'chooseImage',
    //         'previewImage',
    //         'uploadImage',
    //         'downloadImage',
    //         'getNetworkType',
    //         'openLocation',
    //         'getLocation',
    //         'hideOptionMenu',
    //         'showOptionMenu',
    //         'closeWindow',
    //         'scanQRCode',
    //         'chooseWXPay',
    //         'openProductSpecificView',
    //         'addCard',
    //         'chooseCard',
    //         'openCard'
    //     ]
    // });

    var amount = 0;
    var totalPrice =0.00;
    /**
     * inti app
     */
    OnlineOrder.prototype.initApp = function () {
        var self = this;
        console.log('--------------start--------------')
        self.storeSearch = new UI.InputText('searchInput', null, function () {
            var storeSearchVal = self.storeSearch.getToEmptyValue();
            if (null == storeSearchVal || '' == storeSearchVal || undefined == storeSearchVal) {
                $('#ducheng').show();
                $('#juzi').show();
            } else if ('都城快餐'.indexOf(storeSearchVal) >= 0) {
                $('#ducheng').show();
                $('#juzi').hide();
            } else if ('橘子红了西餐餐'.indexOf(storeSearchVal) >= 0) {
                $('#ducheng').hide();
                $('#juzi').show();
            } else {
                $('#ducheng').hide();
                $('#juzi').hide();
            }
        });
        self.btnInit();
    };
    /**
     *   init click event
     */
    OnlineOrder.prototype.btnInit = function () {
        //点击餐厅列表item
        $('#ducheng').click(function () {
            //隐藏餐厅列表、显示餐厅的首页
            $('#page_main').hide();
            $('#page_store').show();

            $('#store_home_page').show();
            $('#price_toolbar').hide();
            $('#store_toolbar').show();
            //餐厅4个界面的显示与隐藏
            $('#store_home').show();
            $('#store_menu').hide();
            $('#store_shopping_cart').hide();
            $('#store_consumer_info').hide();
            //底部工具栏的选中效果
            $('#li_store_home').find('span').addClass('selected-tab-color');
            $('#li_store_home').parent().siblings().find("a").find('span').each(function () {
                $(this).removeClass('selected-tab-color');
            });
        });

        //click bottom toolbar
        $("#ul-bar li").each(function () {
            $(this).click(function () {

                if($(this).index()==0 || $(this).index()==3){
                    $('#price_toolbar').hide();
                }else{
                    $('#price_toolbar').show();
                    if(amount == 0){
                        $('#selected_food_div').hide();
                        $('#empty_cart').show();
                    }else{
                        $('#selected_food_div').show();
                        $('#empty_cart').hide();
                    }
                }
                var url = $(this).find("a").attr("href");
                //hide
                $('.tab-div').each(function () {
                    if (("#" + $(this).attr("id")) == url) {
                        $(this).show();
                    } else {
                        if ($(this).hasClass('tab-div')) {
                            $(this).hide();
                        }

                    }
                });
                //tab style control
                $('.elected-tab-color').each(function () {
                    $(this).removeClass('selected-tab-color');
                });
                $(this).find("a").find('span').addClass('selected-tab-color');
                $(this).siblings().find("a").find('span').each(function () {
                    $(this).removeClass('selected-tab-color');
                });

            });

        });
        //click food item，show food details
        $('.div-menu-right').each(function () {
            $(this).click(function () {
                $('#page_food_detail').show();
                $('#page_store').hide();
            });
        });
        //click shopping car
        /*$('.div-add-padding').each(function () {
            $(this).click(function (event) {
                amount = amount +1;
                totalPrice =parseFloat(totalPrice) +parseFloat(15.00);
                $('#amount').text("共"+amount+"份");
                $('#foodAmount').text(amount);
                $('#total-price').text("价格：￥"+totalPrice.toFixed(2)+"元");

                $('#goto_paymenu').show();
                // 如果在详细页点击添加，则关闭详情页
                if($('#page_food_detail').css('display') != 'none'){
                    $('#page_food_detail').hide();
                    $('#page_store').show();
                }
                event.stopPropagation();
            });
        });*/

        $('.icon-del').each(function(){
            $(this).click(function (event) {
                if(amount > 0 && parseFloat(totalPrice) >=15){
                    amount = amount -1;
                    totalPrice =parseFloat(totalPrice) -parseFloat(15.00);
                    $('#amount').text("共"+amount+"份");
                    $('#foodAmount').text(amount);
                    $('#total-price').text("价格：￥"+totalPrice.toFixed(2)+"元");
                    $('#pay_money').text(totalPrice.toFixed(2));

                    // 如果在详细页点击添加，则关闭详情页
                    if($('#page_food_detail').css('display') != 'none'){
                        $('#page_food_detail').hide();
                        $('#page_store').show();
                    }

                    if(amount == 0){
                        $('#selected_food_div').hide();
                        $('#empty_cart').show();
                        $('#goto_paymenu').hide();
                    }else{
                        $('#goto_paymenu').show();
                        $('#selected_food_div').show();
                        $('#empty_cart').hide();
                    }
                }else{
                    $('#selected_food_div').hide();
                    $('#empty_cart').show();
                }

                event.stopPropagation();
            });

        });

        $('.icon-add').each(function(){
            $(this).click(function (event) {
                amount = amount +1;
                totalPrice =parseFloat(totalPrice) +parseFloat(15.00);
                $('#amount').text("共"+amount+"份");
                $('#foodAmount').text(amount);
                $('#total-price').text("价格：￥"+totalPrice.toFixed(2)+"元");
                $('#pay_money').text(totalPrice.toFixed(2));

                $('#goto_paymenu').show();
                // 如果在详细页点击添加，则关闭详情页
                if($('#page_food_detail').css('display') != 'none'){
                    $('#page_food_detail').hide();
                    $('#page_store').show();
                }
                $('#selected_food_div').show();
                $('#empty_cart').hide();
                event.stopPropagation();
            });
        });

        //click title bar to go back
        $('#title-bar-back').click(function () {
            $('#page_food_detail').hide();
            $('#page_store').show();
        });

        //跳转到结算界面
        $('#goto_paymenu').click(function () {
            $('#store_toolbar').hide();
            $('#page_store').hide();
            $('#pay_order').show();
        });

        $('#confirm_pay').click(function () {
            $('#pay_result').show();
            $('#pay_order').hide();
            amount =0;
            totalPrice =0.00;
            $('#amount').text("共"+amount+"份");
            $('#foodAmount').text(amount);
            $('#total-price').text("价格：￥"+totalPrice+"元");
            $('#goto_paymenu').hide();
            $('#selected_food_div').hide();
        });
        $('#goahead').click(function () {
            $('#pay_result').hide();
            $('#page_main').show();

        });
        $('#myorder').click(function () {
            $('#page_store').hide();
            $('#order_query').show();

        });
        $('#li_store_consumer_info').click(function () {
            HTTP.callWebService({
                'service': '/onlineordering/getAccessToken',
                'params': {},
                'showLoading': false,
                'async':false,
                'dataType':'text',
                'success': function(result){
                    alert(result);
                },
                'error': function (XMLHttpRequest, textStatus, errorThrown) {//请求失败处理函数
                    console.log("请求失败，无法获取分组数据");
                }
            });


        });
    };

    return new OnlineOrder();
});
