/**
 * Created by zhangxue 2017年03月27日14:25:28
 * Version 1.0
 */
define({
    'dummy' : false,
    'debug' : true,
    'browser' : false,

    'page' : {
        'main' : {
            'id' : 'page_main',
            'title' : '微餐厅'
        },
        'confirm' : {
            'id' : 'page_info_pay',
            'title' : '购物车',
            'goToRecords' : true
        },
        'pay' : {
            'id' : 'pay_way',
            'title' : '选择付款方式'
        },
        'float_dia' : {
            'id' : 'page_float_dia',
            'title' : '订单确认',
            'togo' : true
//            'prompt' : true,
//            'isDisable':true
        }
    },

    'CARDTYPECODE' : '2004',

    'CODE' : {
        'COMMOSSION_ERROR' : '004010'
    },
    'ResultMSG' : {
        '006781' : '查询不到该号码的状态，请确认号码无误或稍后再试'
    }
});