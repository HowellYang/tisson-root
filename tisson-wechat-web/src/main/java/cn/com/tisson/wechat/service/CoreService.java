package cn.com.tisson.wechat.service;


import cn.com.tisson.wechat.bean.MessageUtil;
import cn.com.tisson.wechat.bean.message.TextMessage;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


public class CoreService {
	public static String processRequest(HttpServletRequest request){
		//解析微信服务器发送过来的请求
		TextMessage textMessage =new TextMessage();
		try {
			Map<String, String> requestMap = MessageUtil.parseXml(request);
			String toUserName = requestMap.get("ToUserName");
			String fromUserName = requestMap.get("FromUserName");
			String msgType = requestMap.get("MsgType");
			//处理消息并返回处理结果信息
			textMessage.setFromUserName(toUserName);
			textMessage.setToUserName(fromUserName);
			textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
			textMessage.setCreateTime(new Date().getTime());
			if(MessageUtil.REQ_MESSAGE_TYPE_TEXT.equals(msgType)){
				if("time".equals(requestMap.get("Content"))){
					textMessage.setContent(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
				}else if("1".equals(requestMap.get("Content"))){
					textMessage.setContent("http://tissonvx.applinzi.com/onlineordering/onlineordering.html");
				}else{
					textMessage.setContent("您发送的是文本消息");
				}
				
			}else if(MessageUtil.REQ_MESSAGE_TYPE_VOICE.equals(msgType)){
				textMessage.setContent("您发送的是语音消息");
			}else if(MessageUtil.EVENT_TYPE_CLICK.equals(msgType)){
				if(MessageUtil.EVENT_TYPE_SUBSCRIBE.equals(requestMap.get("Event"))){
					textMessage.setContent("欢迎你的订阅");
				}else if(MessageUtil.EVENT_TYPE_UNSUBSCRIBE.equals(requestMap.get("Event"))){
					
				}
			}else if(MessageUtil.REQ_MESSAGE_TYPE_EVENT.equals(msgType)){
                if(MessageUtil.EVENT_TYPE_VIEW.equals(msgType)){
                    textMessage.setContent("openid="+fromUserName);
                }
            }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return MessageUtil.MessageToXml(textMessage);
		
	}
}
