package cn.com.tisson.wechat.controller;

import cn.com.tisson.wechat.service.CoreService;
import cn.com.tisson.wechat.service.WxService;
import cn.com.tisson.wechat.util.AccessTokenTool;
import cn.com.tisson.wechat.util.SignUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;

/**
 * Created by HowellYang on 29/3/17 PM1:14.
 * E-Mail:th15817161961@gmail.com
 */
@Controller
public class IndexController {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    public IndexController() {
        logger.info("=====================初始化============");
    }

    @RequestMapping(value = "/")
    public void index(PrintWriter printWriter) {
        printWriter.print("the tisson wechat system running!");
    }

    @RequestMapping(value = "/vxCoreServlet", method = RequestMethod.GET)
    public
    @ResponseBody
    String doGet(HttpServletRequest request) {
        //判断是否微信内置浏览器
//        if (CommonUtils.isMircoMessenger(request)) {

            // 微信加密签名
            String signature = request.getParameter("signature");
            // 时间戳
            String timestamp = request.getParameter("timestamp");
            // 随机数
            String nonce = request.getParameter("nonce");
            // 随机字符串
            String echostr = request.getParameter("echostr");

            // 通过检验signature对请求进行校验，若校验成功则原样返回echostr，表示接入成功，否则接入失败
            if (SignUtil.checkSignature(signature, timestamp, nonce)) {
                return echostr;
            }
//        }
        return "";
    }

    @RequestMapping(value = "/vxCoreServlet", method = RequestMethod.POST)
    public
    @ResponseBody
    String doPost(HttpServletRequest request) {
        //判断是否微信内置浏览器
//        if(CommonUtils.isMircoMessenger(request)){
        // 微信加密签名
        String signature = request.getParameter("signature");
        // 时间戳
        String timestamp = request.getParameter("timestamp");
        // 随机数
        String nonce = request.getParameter("nonce");
        //请求校验
        if (SignUtil.checkSignature(signature, timestamp, nonce)) {
            // TODO 消息的处理
            String respXml = CoreService.processRequest(request);
            return respXml;
        }
//        }

        return "";
    }

    /**
     * 获取accessToken
     * @param request
     * @return
     */
    @RequestMapping(value = "/onlineordering/getAccessToken")
    public
    @ResponseBody
    String getAccessToken(HttpServletRequest request) {
        String token =AccessTokenTool.getAccessToken();
        return token;
    }

    /**
     * 获取prepayid
     * @param request
     * @return
     */
    @RequestMapping(value = "/onlineordering/getPrePayId")
    public
    @ResponseBody
    String getPrePayId(HttpServletRequest request) {
        String prePayId = WxService.getPrePayid();
        return prePayId;
    }
}
