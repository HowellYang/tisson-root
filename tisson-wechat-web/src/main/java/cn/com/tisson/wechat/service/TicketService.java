package cn.com.tisson.wechat.service;

import cn.com.tisson.wechat.setting.AppProperties;
import cn.com.wechat.api.TicketAPI;
import cn.com.wechat.bean.ticket.Ticket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by HowellYang on 17/4/17 PM2:46.
 * E-Mail:th15817161961@gmail.com
 */
@Slf4j
@Component
public class TicketService implements Serializable {

    private final String WX_TICKET_KEY = "WX_REDIS_TICKET_KEY";

    private final long outTime = 7200;

    @Autowired
    AppProperties appProperties;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    String ticket;

    public String getTicket(String access_token) {
        if(redisTemplate.hasKey(this.WX_TICKET_KEY)){
            return redisTemplate.opsForValue().get(this.WX_TICKET_KEY).toString();
        } else {
            Ticket ticketRsop = TicketAPI.ticketGetticket(access_token,"jsapi");
            if(ticketRsop.isSuccess()){
                ticket = ticketRsop.getTicket();
                this.setTicket(ticket);
            } else {
                ticket = null;
                log.error("获取ticket失败！");
            }
            return ticket;
        }
    }

    public void setTicket(String token) {
        redisTemplate.opsForValue().set(this.WX_TICKET_KEY, token, outTime, TimeUnit.SECONDS);
        this.ticket = token;
    }
}
