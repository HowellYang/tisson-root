package cn.com.tisson.wechat.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Random;

import static cn.com.tisson.wechat.util.Constants.ALLCHAR;

/**
 * Created by zhangxue on 2017/4/7.
 */
public class CommonUtils {
    private final static Logger log = LoggerFactory.getLogger(CommonUtils.class);

    /**
     * 判断是否是微信内置浏览器
     *
     * @param request
     * @return
     */
    public static boolean isMircoMessenger(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        if (null != userAgent && userAgent.contains("MircoMessenger")) {
            return true;
        }
        return false;
    }

    /**
     * 返回一个32长度的随机字符串(只包含大小写字母、数字)
     *
     * @return 随机字符串
     */
    public static String getRandom() {
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < 32; i++) {
            sb.append(ALLCHAR.charAt(random.nextInt(ALLCHAR.length())));
        }
        return sb.toString();
    }

}
