package cn.com.tisson.wechat.util;

/**
 * Created by zhangxue on 2017/4/11.
 */
public class Constants {
    // 与接口配置信息中的Token要一致
    public static String token = "tissonwx";
    public static final String ALLCHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String AppId = "";
    public static final String AppSecret = "";
    public static final String mch_id = "";
    //支付密钥
    public static final String payKey = "";

    public static String BASE_URL="https://api.weixin.qq.com";
    /**
     * token地址
     */
    public static final String GET_ACCESS_TOKEN_URL = BASE_URL+"/cgi-bin/token?grant_type=client_credential&appid="
            + AppId + "&secret=" + AppSecret;


    public static final String POST_ACCESS_TOKEN_URL = BASE_URL+"/cgi-bin/token?grant_type=client_credential";

    public static String tokenparam="{'appid':" + AppId + ",'secret':" + AppSecret+"}";
    public static final String GET_OPENID_URL = BASE_URL+"/sns/oauth2/access_token?grant_type=authorization_code&appid="
            + AppId + "&secret=" + AppSecret;


}
