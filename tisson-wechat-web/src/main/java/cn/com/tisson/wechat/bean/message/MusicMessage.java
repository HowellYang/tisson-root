package cn.com.tisson.wechat.bean.message;


import cn.com.tisson.wechat.bean.message.subbean.Music;

public class MusicMessage extends BaseMessage{
	private Music music;

	public Music getMusic() {
		return music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}
	
}
