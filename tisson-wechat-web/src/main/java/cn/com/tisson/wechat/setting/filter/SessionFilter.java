package cn.com.tisson.wechat.setting.filter;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by yfzx_gd_yanghh on 2016/10/14.
 */
@WebFilter(
        filterName = "SessionFilter",
        urlPatterns = {"/*"}
)
@Slf4j
public class SessionFilter extends OncePerRequestFilter {

    private final Pattern rewritePattern = Pattern.compile("/sites/[^/]+/");

    /**
     * 会话ID
     */
    private final static String SESSION_KEY = "sessionId";

    public SessionFilter(){

    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        Boolean doFilter = false;
        HttpSession session = httpServletRequest.getSession();

        // 请求的uri
        String requestURI = httpServletRequest.getRequestURI();
        Matcher matcher = rewritePattern.matcher(requestURI);
        requestURI = matcher.replaceAll("/");
        String method = httpServletRequest.getMethod().toLowerCase();

        if (!requestURI.contains("/healthcheck.html") && !method.equals("get")){
            log.info("请求: { url:{}, method:{} }" , httpServletRequest.getRequestURL()+"?" + httpServletRequest.getQueryString(),method);
        }

        //微信授权的url上带有code , 可以拿code获取openid；
        String code = "", params = httpServletRequest.getQueryString();

        if(params != null && params.contains("code=")){
            Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();

            String[] values = parameterMap.get("code");
            for (int i = 0; i < values.length; i++) {
                code = values[i];
            }
            log.info("code = " + code);
            session.setAttribute("WX_CODE", code);
        }

        MDC.put(SESSION_KEY, session.getId());


        // 如果不执行过滤，则继续
        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }


    public void requestLog(HttpServletRequest httpServletRequest){
        JSONObject object = new JSONObject();
        object.put("浏览器基本信息",httpServletRequest.getHeader("user-agent"));
        object.put("客户端系统名称",System.getProperty("os.name"));
        object.put("客户端系统版本",System.getProperty("os.version"));
        object.put("客户端操作系统位数",System.getProperty("os.arch"));
        object.put("HTTP协议版本",httpServletRequest.getProtocol());
        object.put("请求编码格式",httpServletRequest.getCharacterEncoding());
        object.put("Accept",httpServletRequest.getHeader("Accept"));
        object.put("Accept-语言",httpServletRequest.getHeader("Accept-Language"));
        object.put("Accept-编码",httpServletRequest.getHeader("Accept-Encoding"));
        object.put("Connection",httpServletRequest.getHeader("Connection"));
        object.put("Cookie",httpServletRequest.getHeader("Cookie"));
        object.put("客户端发出请求时的完整URL",httpServletRequest.getRequestURL().toString());
        object.put("请求行中的资源名部分",httpServletRequest.getRequestURI());
        object.put("请求行中的参数部分",httpServletRequest.getRemoteAddr());
        object.put("客户机所使用的网络端口号",httpServletRequest.getRemotePort());
        object.put("认证的用户",httpServletRequest.getRemoteUser());
        object.put("WEB服务器的IP地址",httpServletRequest.getLocalAddr());
        object.put("WEB服务器的主机名",httpServletRequest.getLocalName());
        object.put("客户机请求方式",httpServletRequest.getMethod());
        object.put("请求的文件的路径",httpServletRequest.getServerName());
        object.put("请求中所有参数的名字",httpServletRequest.getParameterNames().toString());
        object.put("getContentType",httpServletRequest.getContentType());
        object.put("getContextPath",httpServletRequest.getContextPath());
        object.put("getPathInfo",httpServletRequest.getPathInfo());
        object.put("是否使用了加密通道",httpServletRequest.isSecure());
        object.put("服务器端口号",httpServletRequest.getServerPort());
        object.put("SessionId",httpServletRequest.getRequestedSessionId());
        object.put("getAuthType",httpServletRequest.getAuthType());


        Enumeration enumNames= httpServletRequest.getParameterNames();
        int i = 0;
        while (enumNames.hasMoreElements()) {
            String key = (String) enumNames.nextElement();
            i++;
            object.put("参数名称"+i,key);
        }
        log.info(object.toString());
    }

}
