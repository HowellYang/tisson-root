package cn.com.tisson.wechat.controller;

import cn.com.tisson.wechat.service.TicketService;
import cn.com.tisson.wechat.service.TokenService;
import cn.com.tisson.wechat.setting.AppProperties;
import cn.com.wechat.util.JsUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by HowellYang on 14/4/17 PM1:23.
 * E-Mail:th15817161961@gmail.com
 */
@Controller
@Slf4j
public class WxJsConfigController {
    @Autowired
    AppProperties appProperties;

    @Autowired
    TokenService tokenService;

    @Autowired
    TicketService ticketService;

    /**
     * 微信JSAPI 授权配置
     * @param model
     * @param session
     * @return
     */
    @RequestMapping(value = "/getConfigJson", method = RequestMethod.POST)
    public @ResponseBody String getConfigJson(@RequestBody Map<String, Object> model, HttpSession session) {
        JSONObject json = new JSONObject();
        for (Map.Entry<String, Object> entry : model.entrySet()) {
            json.put( entry.getKey() , entry.getValue());
        }
        if(!json.has("url")){
            return "url is null";
        }
        String url = json.getString("url");
        log.info("url:"+url);
        try {
            //缓存在redis上，7200秒
            String access_token = tokenService.getToken();
            //缓存在redis上，7200秒
            String ticket = ticketService.getTicket(access_token);

            return JsUtil.generateConfigJson(ticket, true, appProperties.AppId, url, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }



}
