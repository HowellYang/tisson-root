package cn.com.tisson.wechat.service;

import cn.com.tisson.wechat.setting.AppProperties;
import cn.com.wechat.api.SnsAPI;
import cn.com.wechat.bean.sns.SnsToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;

/**
 * Created by HowellYang on 17/4/17 PM3:57.
 * E-Mail:th15817161961@gmail.com
 */
@Component
@Slf4j
public class OpenIdService {

    public static final String WX_OPENID = "WX_OPENID";

    public static final String WX_CODE = "WX_CODE";

    @Autowired
    AppProperties appProperties;

    String openid;

    public String getOpenid(HttpSession session, String code) {
        if(session.getAttribute(this.WX_OPENID) != null){
            openid = session.getAttribute(this.WX_OPENID) .toString();
        } else {
            if(code == null && session.getAttribute(this.WX_CODE) == null){
                log.info("code 不能为空！");
                //todo : 跳转去授权
                return null;
            } else if(session.getAttribute(this.WX_CODE) != null){
                code = session.getAttribute(this.WX_CODE).toString();
            }
            SnsToken snsToken = SnsAPI.oauth2AccessToken(appProperties.AppId, appProperties.AppSecret, code);
            if(snsToken.isSuccess()){
                openid = snsToken.getOpenid();
                session.setAttribute(this.WX_OPENID, openid);
            }
        }
        return openid;
    }
}
