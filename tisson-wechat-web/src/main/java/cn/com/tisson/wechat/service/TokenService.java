package cn.com.tisson.wechat.service;

import cn.com.tisson.wechat.setting.AppProperties;
import cn.com.wechat.api.TokenAPI;
import cn.com.wechat.bean.token.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by HowellYang on 17/4/17 PM2:30.
 * E-Mail:th15817161961@gmail.com
 */
@Component
public class TokenService implements Serializable {

    private final String WX_TOKE_KEY = "WX_REDIS_TOKE_KEY";

    private final long outTime = 7200;

    @Autowired
    AppProperties appProperties;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    String token;

    public String getToken() {
        if(redisTemplate.hasKey(this.WX_TOKE_KEY)){
            return redisTemplate.opsForValue().get(this.WX_TOKE_KEY).toString();
        } else {
            Token tokenRsop = TokenAPI.token(appProperties.AppId, appProperties.AppSecret);
            token = tokenRsop.getAccess_token();
            this.setToken(token);
            return token;
        }
    }

    public void setToken(String token) {
        redisTemplate.opsForValue().set(this.WX_TOKE_KEY, token, outTime, TimeUnit.SECONDS);
        this.token = token;
    }
}
