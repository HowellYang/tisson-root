package cn.com.tisson.wechat.bean.message;


import cn.com.tisson.wechat.bean.message.subbean.Image;

public class ImageMessage extends BaseMessage{
	private Image image ;

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	
}
