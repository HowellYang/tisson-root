package cn.com.tisson.wechat.service;

import cn.com.tisson.wechat.setting.AppProperties;
import cn.com.wechat.api.PayMchAPI;
import cn.com.wechat.bean.paymch.Unifiedorder;
import cn.com.wechat.bean.paymch.UnifiedorderResult;
import cn.com.wechat.util.PayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * Created by HowellYang on 17/4/17 PM4:49.
 * E-Mail:th15817161961@gmail.com
 */
@Service
public class PrepayIdService {
    @Autowired
    AppProperties appProperties;

    public String getPrepayId(String openid, HttpServletRequest request){

        Unifiedorder unifiedorder = new Unifiedorder();
        unifiedorder.setAppid(appProperties.AppId);
        unifiedorder.setMch_id(appProperties.mch_id);
        unifiedorder.setNonce_str(UUID.randomUUID().toString().replace("-", ""));

        unifiedorder.setBody("商品信息");
        unifiedorder.setOut_trade_no("20"+ System.currentTimeMillis() / 1000L);

        //todo :查询订单金额
        unifiedorder.setTotal_fee("1");//单位分
        unifiedorder.setSpbill_create_ip(request.getRemoteAddr());//IP
        unifiedorder.setNotify_url("http://mydomain.com/test/notify");
        unifiedorder.setTrade_type("JSAPI");//JSAPI，NATIVE，APP，MWEB
        unifiedorder.setOpenid(openid);

        UnifiedorderResult unifiedorderResult = PayMchAPI.payUnifiedorder(unifiedorder, appProperties.payKey);

        //@since 2.8.5  API返回数据签名验证
        if(unifiedorderResult.getSign_status() !=null && unifiedorderResult.getSign_status()){
            String json = PayUtil.generateMchPayJsRequestJson(unifiedorderResult.getPrepay_id(), appProperties.AppId, appProperties.payKey);
            System.out.println("**************** Prepay_id =" + unifiedorderResult.getPrepay_id());
            return unifiedorderResult.getPrepay_id();
        }
        return "";
    }
}
