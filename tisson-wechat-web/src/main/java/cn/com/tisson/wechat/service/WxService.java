package cn.com.tisson.wechat.service;

import cn.com.tisson.wechat.util.Constants;
import cn.com.tisson.wechat.util.HttpUtil;
import cn.com.tisson.wechat.util.SignUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by zhangxue on 2017/4/11.
 */
public class WxService {
    private static Logger log = LoggerFactory.getLogger(WxService.class);
    /**
     * 获取accessToken
     * @return
     */
    public static String initAccessToken(){
        JSONObject object =new JSONObject();
        try {
            String responseContent = HttpUtil.sendGet(Constants.GET_ACCESS_TOKEN_URL);
            object = new JSONObject(responseContent);
            return (String) object.get("access_token");
        } catch (JSONException e) {
            log.error("获取token失败 errcode:" + object.get("errcode") + " errmsg:" + object.getString("errmsg"));
            e.printStackTrace();
        } catch (Exception e){
            log.error("获取token失败",e);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取openid
     * @param request
     * @return
     */
    public static String getOpenid(HttpServletRequest request){
        Map<String, String> requestMap = null;
        try {
            /*requestMap = MessageUtil.parseXml(request);
            String code = requestMap.get("code");*/
            String code ="051DdLWW1oQefV08FsVW1AoEWW1DdLWo";
            String url=Constants.GET_OPENID_URL+"&code="+code;
            String responseContent = HttpUtil.sendGet(url);
            JSONObject object = new JSONObject(responseContent);
            return (String) object.get("access_token");

        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取openid失败",e);
        }

        return null;
    }
    /**
     * 统一下单接口获取prePayid
     * @return
     */
    public static String getPrePayid(){
        SortedMap<Object,Object> parameters = new TreeMap<Object,Object>();
        String sign = SignUtil.createSign(parameters);
        return  null;
    }

    /**
     * 创建统一下单的xml的java对象
     * @param bizOrder 系统中的业务单号
     * @param ip 用户的ip地址
     * @param openId 用户的openId
     * @return
     *//*
    public PayInfo createPayInfo(BizOrder bizOrder, String ip, String openId) {
        PayInfo payInfo = new PayInfo();
        payInfo.setAppid(Constants.appid);
        payInfo.setDevice_info("WEB");
        payInfo.setMch_id(Constants.mch_id);
        payInfo.setNonce_str(CommonUtil.create_nonce_str().replace("-", ""));
        payInfo.setBody("这里是某某白米饭的body");
        payInfo.setAttach(bizOrder.getId());
        payInfo.setOut_trade_no(bizOrder.getOrderCode().concat("A").concat(DateFormatUtils.format(new Date(), "MMddHHmmss")));
        payInfo.setTotal_fee((int)bizOrder.getFeeAmount());
        payInfo.setSpbill_create_ip(ip);
        payInfo.setNotify_url(Constants.notify_url);
        payInfo.setTrade_type("JSAPI");
        payInfo.setOpenid(openId);
        return payInfo;
    }*/

}
