package cn.com.tisson.wechat.bean.message;


import cn.com.tisson.wechat.bean.message.subbean.Video;

public class VideoMessage extends BaseMessage{
	private Video video;

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}
	
}
