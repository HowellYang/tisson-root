package cn.com.tisson.wechat.setting;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by HowellYang on 14/4/17 AM10:03.
 * E-Mail:th15817161961@gmail.com
 */
@Component
public class AppProperties {

    @Value("#{apps['wx_app_id']}")
    public String AppId;

    @Value("#{apps['wx_secret']}")
    public String AppSecret;

    @Value("#{apps['wx_mch_id']}")
    public String mch_id;

    //支付密钥
    @Value("#{apps['wx_key']}")
    public String payKey;

    @Value("#{apps['debug']}")
    public String debug;
}
