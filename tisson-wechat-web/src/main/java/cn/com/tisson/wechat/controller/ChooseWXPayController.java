package cn.com.tisson.wechat.controller;


import cn.com.tisson.wechat.service.OpenIdService;
import cn.com.tisson.wechat.service.PrepayIdService;
import cn.com.tisson.wechat.setting.AppProperties;
import cn.com.wechat.api.PayMchAPI;
import cn.com.wechat.bean.pay.PayJsRequest;
import cn.com.wechat.bean.paymch.Unifiedorder;
import cn.com.wechat.bean.paymch.UnifiedorderResult;
import cn.com.wechat.util.JsonUtil;
import cn.com.wechat.util.MapUtil;
import cn.com.wechat.util.PayUtil;
import cn.com.wechat.util.SignatureUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.UUID;

/**
 * Created by HowellYang on 13/4/17 AM9:39.
 * E-Mail:th15817161961@gmail.com
 */
@Controller
public class ChooseWXPayController {

    @Autowired
    AppProperties appProperties;

    @Autowired
    OpenIdService openIdService;

    @Autowired
    PrepayIdService prepayIdService;
    /**
     * JS发起一个微信支付请求
     * @return json
     */
    @RequestMapping(value = "/payJSServlet", method = RequestMethod.POST)
    public @ResponseBody String getPayJSJson(HttpServletRequest request, HttpSession session){
        //拿openid获取prepay_id
        String prepay_id = prepayIdService.getPrepayId(openIdService.getOpenid(session, null), request);

        String package_ = "prepay_id="+prepay_id;
        PayJsRequest payJsRequest = new PayJsRequest();
        payJsRequest.setAppId(appProperties.AppId);
        payJsRequest.setNonceStr(UUID.randomUUID().toString().replace("-", ""));
        payJsRequest.setPackage_(package_);
        payJsRequest.setSignType("MD5");
        payJsRequest.setTimeStamp(System.currentTimeMillis()/1000+"");

        Map<String, String> mapS = MapUtil.objectToMap(payJsRequest);
        String paySign = SignatureUtil.generateSign(mapS, appProperties.payKey);
        payJsRequest.setPaySign(paySign);
        return JsonUtil.toJSONString(payJsRequest);
    }

    /**
     * WcPay 方式
     * @param request
     * @param session
     * @return
     */
    @RequestMapping(value = "/payMchPayServlet", method = RequestMethod.POST)
    public @ResponseBody String getMchPayJsJson(HttpServletRequest request, HttpSession session){


        //拿openid获取prepay_id
        String prepay_id = prepayIdService.getPrepayId(openIdService.getOpenid(session, null), request);

        Unifiedorder unifiedorder = new Unifiedorder();
        unifiedorder.setAppid(appProperties.AppId);
        unifiedorder.setMch_id(appProperties.mch_id);
        unifiedorder.setNonce_str(UUID.randomUUID().toString().replace("-", ""));

        unifiedorder.setBody("商品信息");
        unifiedorder.setOut_trade_no(System.currentTimeMillis()/1000+"");
        unifiedorder.setTotal_fee("1");//单位分
        unifiedorder.setSpbill_create_ip(request.getRemoteAddr());//IP
        unifiedorder.setNotify_url("http://www.you-and-me.cn/wechat/notify");
        unifiedorder.setTrade_type("JSAPI");//JSAPI，NATIVE，APP，MWEB
        unifiedorder.setOpenid(openIdService.getOpenid(session, null));

        UnifiedorderResult unifiedorderResult = PayMchAPI.payUnifiedorder(unifiedorder, appProperties.payKey);

        //@since 2.8.5  API返回数据签名验证
        if(unifiedorderResult.getSign_status() !=null && unifiedorderResult.getSign_status()){
            String jsonStr = PayUtil.generateMchPayJsRequestJson(unifiedorderResult.getPrepay_id(), appProperties.AppId, appProperties.payKey);
            return jsonStr;
        }
        return "";
    }




}
