package cn.com.tisson.wechat.util;

import cn.com.tisson.wechat.service.WxService;


/**
 * Created by zhangxue on 2017/4/10.
 */
public class AccessTokenTool {



    public static String access_token = "";

    /**
     * 过期时间7200秒， 因为微信token过期时间为2小时，即7200秒
     */
    private static int expireTime = 7200 * 1000;
    private static long refreshTime;

    /**
     * 获取微信accesstoken
     *
     * @return
     */
    public static synchronized String getAccessToken() {
        return getAccessToken(false);
    }

    public static synchronized String getAccessToken(boolean refresh) {
        if (access_token != null || (System.currentTimeMillis() - refreshTime) > expireTime || refresh) {
            access_token = WxService.initAccessToken();
            refreshTime = System.currentTimeMillis();
        }
        return access_token;
    }


}
