package cn.com.tisson.wechat.setting.spring;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by yfzx_gd_yanghh pec_masteron 2016/9/28.
 */
@Configuration
@PropertySource(name = "app" , value = {"classpath:properties/app.properties"})
@ImportResource({"classpath:properties/spring/root_config.xml" } )
public class SpringRootConfig {
}
