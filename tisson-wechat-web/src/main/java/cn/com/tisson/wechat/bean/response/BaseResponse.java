package cn.com.tisson.wechat.bean.response;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by zhangxue on 2017/4/12.
 */
@Data
public class BaseResponse implements Serializable {
    private static final long serialVersionUID = -3705810713004060640L;
    private String return_code ;
    private String return_msg ;

}
