package com.tisson.wechat.test;

import cn.com.tisson.wechat.service.WxService;
import cn.com.tisson.wechat.util.AccessTokenTool;
import cn.com.tisson.wechat.util.CommonUtils;
import org.junit.Test;

/**
 * Created by zhangxue on 2017/4/10.
 */
public class WXTest {
    @Test
     public void testGetAccessToken() {
        String token = AccessTokenTool.getAccessToken();
        System.out.println("**************** token =" + token);
    }
    @Test
    public void testGetRandom(){
        System.out.println(CommonUtils.getRandom());
    }

    @Test
    public  void testgetOpenid(){
        System.out.println(WxService.getOpenid(null));
    }
}
