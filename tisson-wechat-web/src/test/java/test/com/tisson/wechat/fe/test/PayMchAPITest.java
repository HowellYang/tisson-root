package test.com.tisson.wechat.fe.test;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.junit.Test;
import test.com.tisson.wechat.fe.utils.Config;
import cn.com.wechat.api.API;
import cn.com.wechat.api.PayMchAPI;
import cn.com.wechat.bean.paymch.Unifiedorder;
import cn.com.wechat.bean.paymch.UnifiedorderResult;
import cn.com.wechat.bean.user.FollowResult;
import cn.com.wechat.client.LocalHttpClient;
import cn.com.wechat.util.PayUtil;

import java.util.UUID;

/**
 * Created by HowellYang on 12/4/17 PM2:49.
 * E-Mail:th15817161961@gmail.com
 */
public class PayMchAPITest {
    private String appid = "";			//appid
    private String mch_id = "";      	//微信支付商户号
    private String key = "";				//支付API密钥
    private String secret = ""; //微信密钥

    @Test
    public void getAccess_token() throws Exception{
        //TokenManager.init(appid, "secret");
        String token = Config.initAccessToken(appid, secret);
        System.out.println("**************** token =" + token);
    }



    @Test
    public void getUserGet() throws Exception{
        HttpUriRequest httpUriRequest = RequestBuilder.get().setUri("https://api.weixin.qq.com/cgi-bin/user/get").
                addParameter("access_token", API.accessToken(Config.initAccessToken(appid, secret))).
                addParameter("next_openid", "").build();
        FollowResult followResult = (FollowResult) LocalHttpClient.executeJsonResult(httpUriRequest, FollowResult.class);
        String[] strings =  followResult.getData().getOpenid();

        for (int i = 0; strings.length > i; i++){
            System.out.println(strings[i]);
        }

    }

    @Test
    public void PayMchAPITest() {
        Unifiedorder unifiedorder = new Unifiedorder();
        unifiedorder.setAppid(appid);
        unifiedorder.setMch_id(mch_id);
        unifiedorder.setNonce_str(UUID.randomUUID().toString().replace("-", ""));

        unifiedorder.setBody("商品信息");
        unifiedorder.setOut_trade_no("123456");
        unifiedorder.setTotal_fee("1");//单位分
        unifiedorder.setSpbill_create_ip("172.0.0.1");//IP
        unifiedorder.setNotify_url("http://mydomain.com/test/notify");
        unifiedorder.setTrade_type("JSAPI");//JSAPI，NATIVE，APP，MWEB
        unifiedorder.setOpenid("o-ErSwkVVfDBIRUqVsNTHw65kPJA");

        UnifiedorderResult unifiedorderResult = PayMchAPI.payUnifiedorder(unifiedorder, key);

        //@since 2.8.5  API返回数据签名验证
        if(unifiedorderResult.getSign_status() !=null && unifiedorderResult.getSign_status()){
            String json = PayUtil.generateMchPayJsRequestJson(unifiedorderResult.getPrepay_id(), appid, key);

            System.out.println("**************** Prepay_id =" + unifiedorderResult.getPrepay_id());
        }
    }
}
