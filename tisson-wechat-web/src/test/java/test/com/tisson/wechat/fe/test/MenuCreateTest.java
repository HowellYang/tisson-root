package test.com.tisson.wechat.fe.test;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import test.com.tisson.wechat.fe.utils.Config;
import cn.com.wechat.api.MenuAPI;
import cn.com.wechat.bean.BaseResult;

/**
 * Created by HowellYang on 13/4/17 PM4:56.
 * E-Mail:th15817161961@gmail.com
 */
public class MenuCreateTest {

    @Test
    public void menuCreateTest() throws Exception{
        JSONObject json = JSONObject.parseObject("{\n" +
                "    \"button\": [\n" +
                "        {\n" +
                "            \"type\": \"click\",\n" +
                "            \"name\": \"图文获取\",\n" +
                "            \"key\": \"图文\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"type\": \"view\",\n" +
                "            \"name\": \"登录2\",\n" +
                "            \"url\": \"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx4bf3a850afcdd223&redirect_uri=http://www.you-and-me.cn/wechat/demo/demo.html&response_type=code&scope=snsapi_base&state=123456#wechat_redirect\"\n" +
                "        }\n" +
                "    ]\n" +
                "}");

        BaseResult baseResult = MenuAPI.menuCreate(Config.initAccessToken(Config.appid, Config.secret),json.toJSONString());
        System.out.println(baseResult.toString());
    }
}
