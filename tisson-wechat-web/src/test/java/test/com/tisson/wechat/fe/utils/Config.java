package test.com.tisson.wechat.fe.utils;

import cn.com.tisson.wechat.util.HttpUtil;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HowellYang on 13/4/17 PM4:57.
 * E-Mail:th15817161961@gmail.com
 */
public class Config {
    public static String appid = "";			//appid
    public static String mch_id = "";      	//微信支付商户号
    public static String key = "";				//支付API密钥
    public static String secret = ""; //微信密钥

    public static String initAccessToken(String AppId, String AppSecret) throws Exception{
        String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
                + AppId + "&secret=" + AppSecret;
        String responseContent = HttpUtil.sendGet(GET_ACCESS_TOKEN_URL);
        JSONObject object = new JSONObject();
        try {
            object = new JSONObject(responseContent);
            return (String) object.get("access_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
