/**
 * 
 */
package cn.com.wechat.bean.shakearound.device.bindpage;

import cn.com.wechat.bean.BaseResult;

/**
 * 微信摇一摇周边－配置设备与页面的关联关系－响应参数
 * @author Moyq5
 * @date 2016年7月26日
 */
public class DeviceBindPageResult extends BaseResult {

	// private Object data
}
