package cn.com.wechat.bean.user;

import cn.com.wechat.bean.BaseResult;

import java.util.List;

/**
 * 标签
 * 
 * @author SLYH
 * 
 */
public class TagsGetResult extends BaseResult {

	private List<Tag> tags;

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
}
