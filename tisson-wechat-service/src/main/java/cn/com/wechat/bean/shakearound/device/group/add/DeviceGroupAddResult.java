/**
 * 
 */
package cn.com.wechat.bean.shakearound.device.group.add;

import cn.com.wechat.bean.shakearound.AbstractResult;
import cn.com.wechat.bean.shakearound.device.group.GroupInfo;

/**
 * 微信摇一摇周边－新增分组－响应参数
 * @author Moyq5
 * @date 2016年7月30日
 */
public class DeviceGroupAddResult extends AbstractResult<GroupInfo> {

}
