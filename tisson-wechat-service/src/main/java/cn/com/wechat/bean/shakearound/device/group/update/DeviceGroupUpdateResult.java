/**
 * 
 */
package cn.com.wechat.bean.shakearound.device.group.update;

import cn.com.wechat.bean.BaseResult;

/**
 * 微信摇一摇周边－编辑分组信息－响应参数
 * @author Moyq5
 * @date 2016年7月30日
 */
public class DeviceGroupUpdateResult extends BaseResult {
	// private Object data
}
