package cn.com.wechat.bean.message;

import cn.com.wechat.bean.BaseResult;

public class ApiAddTemplateResult extends BaseResult{

	private String template_id;

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	
	
}
