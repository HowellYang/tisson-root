package cn.com.wechat.bean.shakearound.device.group.delete;

import cn.com.wechat.bean.BaseResult;

/**
 * 微信摇一摇周边－删除分组－响应参数
 * @author Moyq5
 * @date 2016年7月30日
 */
public class DeviceGroupDeleteResult extends BaseResult {

	// private Object data
}
