package cn.com.wechat.bean.media;

import cn.com.wechat.bean.BaseResult;


public class UploadimgResult extends BaseResult{

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


}
